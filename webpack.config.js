module.exports = {
    devtool: 'source-map',
    entry: {
        lazio: 'lazio.html',
    },
    mode: 'development',
    module: {
        rules: [{
            exclude: /node_modules/,
            test: /\.js$/
        }]
    },
    output: {
        filename: '[name].bundle.js',
        path: __dirname + '/dist'
    }
}